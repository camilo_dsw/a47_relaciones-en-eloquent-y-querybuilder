<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Channel;

class CommunityLink extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 
        'channel_id', 
        'title', 
        'link', 
        'approved',
      ];

      public function creator()
        {
            return $this->belongsTo(User::class, 'user_id');
        }

        public function channel(){

          return $this->belongsTo(Channel::class);

        }


       // A continuación crea un método static en el modelo que se 
       //llame hasAlreadyBeenSubmitted al que le pasaremos como argumento 
       //el link y en el que ejecutaremos la consulta:
       
        protected static function hasAlreadyBeenSubmitted($link)
        {
              if ($existing = static::where('link', $link)->first()) {
              $existing->touch();
              $existing->save();
              return true;
              }
              return false;
        }


}
