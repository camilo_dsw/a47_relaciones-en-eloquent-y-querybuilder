<ul>
@if(count($links))
@foreach ($links as $link)
<li>
    <a  href="/community/{{ $link->channel->slug }}" >
        <span class="label label-default" style="background: {{ $link->channel->color }}">
            {{ $link->channel->title }}
        </span>
        
    </a>

    <a  href="{{$link->link}}" target="_blank">
        {{ $link->title }}
    </a>

    <small>Contributed by: {{$link->creator->name}} {{$link->updated_at->diffForHumans()}}</small>
</li>
@endforeach
            
@else
<li class="Links__link">
    No contributions yet.
</li>    
@endif
</ul>