<script src="https://cdn.tailwindcss.com"></script>
@extends('layouts.app')
@section('content')


<h1>Listado de la consulta</h1>
    <div class="alert alert-danger">
        <div class='flex items-stretch'>
            @if(count($consulta))
           <div class='py-4'> 
                <h1>Tabla Profile</h1>
                <div class="flex space-x-5">
                    <div> id </div>
                    <div> user_id </div>
                    <div> rol</div>
                </div>
                @foreach ($consulta->all() as $link)
                
                    <div class="flex space-x-10">
                        <div>{{$link->id}}</div>
                        <div>{{$link->user_id}}</div>
                        <div>{{$link->rol}}</div>
                    </div>
                
                    @endforeach
           </div>
                @endif 
        
       
            @if(count($consulta1))
            <div class='py-4 ml-20'>
                    <div class="flex space-x-5">
                        <h1>Tabla Users</h1>
                    </div>
                    <div class="flex space-x-20">
                    
                
                        <div> id </div>
                        <div>  Nombre </div>
                        <div> Email</div>
                    </div>
                    @foreach ($consulta1->all() as $link)
                    
                        <div class="flex space-x-10">
                            <div>{{$link->id}}</div>
                            <div>{{$link->name}}</div>
                            <div>{{$link->email}}</div>
                        </div>
                    
                        @endforeach
             
                    </div>  
                 </div> 
                @endif 
        

                
                    @if(count($consulta2))
                  
                        <h1>Resultado de la consulta</h1>
                        <div class="flex space-x-20">
                            <div> id </div>
                            <div> Nombre</div>
                            <div> rol</div>
                        </div>
                        @foreach ($consulta2->all() as $link)
                        
                            <div class="flex space-x-10">
                                <div>{{$link->id}}</div>
                                <div>{{$link->name}}</div>
                                <div>{{$link->rol}}</div>
                            </div>
                        
                            @endforeach
                  
                    @endif 



    </div>
   