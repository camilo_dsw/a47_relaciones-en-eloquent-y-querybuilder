<?php
use App\Models\CommunityLink;
use App\Models\User;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;





/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Auth::routes();
Auth::routes(['verify' => 'true']);
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['middleware' => 'verified'], function () {

// Rutas a verificar


});
//para mostrar todos los links que llamará al método index mediante GET
Route::get('community', [App\Http\Controllers\CommunityLinkController::class, 'index'])
->middleware('auth');
//para crear un link que llamará al método store del controlador mediante POST
Route::post('community', [App\Http\Controllers\CommunityLinkController::class, 'store'])
->middleware('auth');

//será el método index el encargado de hacer el filtrado dependiendo 
//del parámetro channel
Route::get('community/{channel}', [App\Http\Controllers\CommunityLinkController::class, 'index']);

Route::get('/eloquent', [App\Http\Controllers\CommunityLinkController::class, 'eloquent']);


/*
Route::get('/queryBuilder',function(){

    
     
    $consulta = User::all();

    $consulta2 = User::all();

    // borramos el usuario 7 y su perfil
        //$borrar = User::find(7);
        //$borrar->delete();

    return view('community.queryBuilder', compact('consulta','consulta2'));


  });*/

  Route::get('/queryBuilder',function(){

    $consulta = DB::table('users')->get();
    $consulta1 = DB::table('users')->where('id',3)->delete();
    $consulta2 = DB::table('users')->get();                            
    
    return view('community.queryBuilder', compact('consulta','consulta1','consulta2'));
  });











/*
Route::get('ejemplo', function (Request $request){
    $name = $request->fullUrl();
    return dd($name);
});*/
/*
Route::get('/home', function() {

    return response('Error', 404);
    //->header('Content-type', 'text/plain');

//$consulta2 = DB::table('users')->where('email', 'LIKE', '%laravel%')
    //                   ->orwhere('email', 'LIKE', '%com%')->get();

    $consulta2 = DB::table('users')->insertGetId(
        ['name'=>'Arturo6','email'=>'arturo6@gmail.com','password'=>'12345678']);

 $consulta2 = DB::table('users')->insert([
        ['name'=>'Arturo3','email'=>'arturo3@gmail.com','password'=>'12345678'],
        ['name'=>'Arturo4', 'email'=>'arturo4@gmail.com','password'=>'12345678']]);

     $consulta2 = DB::table('writers')->get();

$consulta2 = DB::table('users')->where('email', 'LIKE', '%laravel%')
                       ->orwhere('email', 'LIKE', '%com%')->get();

    $consulta2 = User::where('email', 'LIKE', '%laravel%')
                       ->orwhere('email', 'LIKE', '%com%')->get();

    return view('community.queryBuilder', compact('consulta','consulta2'));

});*/